#Proyecto de Automatizacion Maven-Selenium-Gauge

##Estructura del proyecto
->Specs
->Driver
->Methods
->Util
->App

##Ejecucion de las pruebas
-Pruebas total
    mvn clean test
-Pruebas specs
    mvn gauge:execute -DspecsDir="specs\docker\02-Reto.spec"

##Generacion de reporte
Proyecto cuenta con un reporte Gauge para las pruebas despues de ejecutarse las pruebas ingresar a la carpeta "reports" y posterior abrir el archivo index.html

##Repositorio
Se genera un repositorio en gitlab para almacenar el codigo fuente