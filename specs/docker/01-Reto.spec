#Calculo de la Hipoteca
*Ir a la url "rocketmortgage"

##Realizar el calculo de la hipoteca 365 meses
*Ingreso de datos "250000" tasa "4" deposito "20" plazo "30"
*Activar la opcion de porcentaje "false"
*Total de cuota de la hipoteca

##Realizar el calculo de la hipoteca 240 meses
*Ingreso de datos "100000" tasa "5" deposito "15" plazo "20"
*Activar la opcion de porcentaje "false"
*Total de cuota de la hipoteca

##Realizar el calculo de la hipoteca 120 meses
*Ingreso de datos "50000" tasa "8" deposito "35" plazo "10"
*Activar la opcion de porcentaje "false"
*Total de cuota de la hipoteca

##Realizar el calculo de la hipoteca cambiar por dolares deposito
*Ingreso de datos "50000" tasa "8" deposito "35" plazo "30"
*Activar la opcion de porcentaje "true"
*Total de cuota de la hipoteca

##Realizar el calculo de la hipoteca valor negativo monto
*Ingreso de datos "-750000" tasa "8" deposito "35" plazo "30"
*Activar la opcion de porcentaje "true"
*Total de cuota de la hipoteca