package com.docker.ngt.lambda.qa.app;

import java.io.IOException;

import com.docker.ngt.lambda.qa.driver.Driver;
import com.thoughtworks.gauge.Step;

public class AppStep {

    private AppPage app;

    public String str;
    public double cuo, inte, depo;
    public int pla;

    @Step("Ir a la url <sitio>")
    public void irUrl(String sitio) throws IOException, Exception{
        switch(sitio.toUpperCase()){
            case "POKEMON":
                this.app = new AppPage(Driver.getDriver(true)).goToPage(System.getenv("POKEMON_URL")).init();
                break;
            case "ROCKETMORTGAGE":
                this.app = new AppPage(Driver.getDriver(true)).goToPage(System.getenv("ROCKETMORTGAGE_URL")).init();
                break;
        }
        this.app.screenShot();
    }

    @Step("Ingreso de datos <cuota> tasa <interes> deposito <deposito> plazo <tiempo>")
    public void ingresoDatosCalcularCuota(Double cuota, Double interes, Double deposito, Integer plazo){
        cuo=cuota;
        inte=interes;
        depo=deposito;
        pla = plazo;
        this.app.calcularCuotaHipoteca(cuota, interes, deposito, plazo);
    }

    @Step("Activar la opcion de porcentaje <opt>")
    public void activarOpcionPorcentaje(Boolean opt){
        this.app.ingresarDatosHipoteca(cuo, inte, depo, pla, opt).screenShot();
    }

    @Step("Total de cuota de la hipoteca")
    public void totalCuotaHipoteca(){
        this.app.verificationCuotaHipoteca().screenShot();
    }

    @Step("Consultar habilidades del siguiente pokemon <value>")
    public void consultarHabilidadesPokemon(String value){
        str=value;
        this.app.consultaHabilidadPokemon(value).screenShot();
    }

     @Step("Consultar stats del siguiente pokemon")
    public void consultarStastPokemon(){
        this.app.consultaStatsPokemon(str).screenShot();
    }
}
