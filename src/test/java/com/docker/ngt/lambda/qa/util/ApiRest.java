package com.docker.ngt.lambda.qa.util;

import static org.junit.Assert.assertTrue;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import org.json.JSONArray;
import org.json.JSONObject;

public class ApiRest {

    public static String[] apiHabilidad = new String[3];
    public static Integer[] apiStats = new Integer[7];
    public static Integer numHabilidad, numStats, numValue;
    
    public void ConsultaApiRest(String str){
        StringBuilder builder = new StringBuilder();
        try{
            // Establecer URL del servidor
            URL url = new URL(System.getenv("POKEMON_API")+str);

            // Abrir conexión HTTP
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setDoOutput(true);

            // Leer respuesta del servidor
            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String linea;
            while ((linea = reader.readLine()) != null) {
                builder.append(linea);
                builder.append(System.lineSeparator());
            }
            reader.close();

            // Convertir respuesta JSON a objeto Java
            JSONObject jsonResponse = new JSONObject(builder.toString());
            //System.out.println(jsonResponse.toString());
            
            JSONArray abilitiesArray = jsonResponse.getJSONArray("abilities");
            // Verificamos si hay al menos un elemento en el array "abilities"
            numHabilidad = abilitiesArray.length();
            System.out.println(numHabilidad);
            if (abilitiesArray.length() > 0) {
                for(int i=0; i<abilitiesArray.length(); i++) {
                    JSONObject firstAbilityObject = abilitiesArray.getJSONObject(i);
                    JSONObject abilityObject = firstAbilityObject.getJSONObject("ability");            
                    apiHabilidad[i] = abilityObject.getString("name");
                    System.out.println("habilidad "+apiHabilidad[i]);
                }    
            }
            
            JSONArray statsArray = jsonResponse.getJSONArray("stats");
            // Verificamos si hay al menos un elemento en el array "abilities"
            if (statsArray.length() > 0) {
                for(int i=0; i<statsArray.length(); i++) {
                    JSONObject statObject = statsArray.getJSONObject(i);
                    apiStats[i] = statObject.getInt("base_stat");
                    //System.out.println("stats "+apiStats[i]);            
                }
            }
        }catch(Exception e){

        }finally{
                
        }
    }

    public void verificacionDatosApiRest(String[] habilidad, Integer[] stats, Integer value){
        String str=null;
        Boolean option;
        //Verificacion de habilidades
        for(int i=0; i<value; i++){
            System.out.println("Valor es: "+apiHabilidad[i].toString());
            if(apiHabilidad[i].toString().replace("-"," ").contentEquals(habilidad[i].toLowerCase().toString())){
                System.out.println("Verificado la habilidad: "+apiHabilidad[i].replace("-", " ").toString());
                option=true;
            }else{
                option=false;
            }
            assertTrue(habilidad[i].toLowerCase().toString()+" habilidad no coincide con el Pokemon", option);
        }
        //Verificacion de stats
        for(int i=0; i<6; i++){
            switch(i){
                case 0:str="HP";break;
                case 1:str="Attack";break;
                case 2:str="Defense";break;
                case 3:str="Sp. Atk";break;
                case 4:str="Sp. Def";break;
                case 5:str="Speed";break;
            }
            if(stats[i]==apiStats[i]){
                System.out.println("Verificado "+str+": "+apiStats[i]);
                option=true;
            }else{
                option=false;
            }  
            assertTrue(str+" no coincide con el Pokemon", option);        
        }
    }
}
