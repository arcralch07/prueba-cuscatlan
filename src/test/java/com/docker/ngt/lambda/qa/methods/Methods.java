package com.docker.ngt.lambda.qa.methods;

import java.time.Duration;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * A test page
 */
public abstract class Methods{

    protected final int MAX_TIME_WAIT;
    protected final int MAX_TIME_WAIT_MODAL;
    protected final int MIN_TIME_WAIT;
    protected final String BROWSER;
    //private static final int FIELD = 0;
    //private static final int VALUE = 1;

    private WebDriver driver;
    
    public Methods(WebDriver driver){
        this.MAX_TIME_WAIT = Integer.parseInt(System.getenv("MAX_TIME_WAIT"));
        this.MAX_TIME_WAIT_MODAL = Integer.parseInt(System.getenv("MAX_TIME_WAIT_MODAL"));
        this.MIN_TIME_WAIT = Integer.parseInt(System.getenv("MIN_TIME_WAIT"));
        this.BROWSER = System.getenv("BROWSER");
        this.driver = driver;
    }

    //Identificador de un WebElement
     protected WebElement el(By by){
        return driver.findElement(by);
    }

    //Identificador de una lista del mismo WebElement
    protected List<WebElement> els(By by){
        return driver.findElements(by);
    }

    //Metodo click WebElement por medio By
    protected void click(By by){
        waitForElement(by);
        clickForElement(el(by));
    }
    
    //Metodo cick WebElement por medio WebElement
    protected void clickForElement(WebElement webElement) {
        try {
            moveForElement(webElement);
            webElement.click();
        } catch (Exception e) {
            Actions actions = new Actions(driver);
            actions.click(webElement).build().perform();
        }

    }

    //Metodo de espera de un WebElement
    protected void waitForElement(By by) {
        WebDriverWait wait = new WebDriverWait(driver(), Duration.ofSeconds(MAX_TIME_WAIT));
        wait.until(ExpectedConditions.or(ExpectedConditions.elementToBeClickable(by),
                ExpectedConditions.presenceOfElementLocated(by), ExpectedConditions.elementToBeSelected(by)));
    }

    //Metodo de moverse hasta WebElement
    protected void moveForElement(WebElement element){
        Actions actions = new Actions(driver);
        actions.doubleClick(element).build().perform();
    }

    //Metodo de escritura de un WebElement
    protected void sendKeysElement(By by, String str){
        click(by);
        sendKeysElement(el(by), str);
    }

    protected void sendKeysElement(WebElement element, String str){
        element.clear();
        element.sendKeys(str);
        element.submit();
    }

    protected void wt(long millis){
        try{
            Thread.sleep(millis);
        }catch (InterruptedException e){
            e.printStackTrace();
        }
    }

    protected void wait(int seconds){
        wt(1000 * seconds);
    }

    protected WebDriver driver() {
        return this.driver;
    }

    protected void executeScript(String script){
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript(script);
    }

    protected void windowForScroll(int x, int y){
        executeScript("window.scrollBy("+ x +","+ y +")");
    }

     //Metodo de tipo WebElement
     protected By createBy(String typeSelector, String query) throws Exception {
        By byObject = null;
        switch (typeSelector) {
            case "cssSelector":
                byObject = new By.ByCssSelector(query);
                break;
            case "xpath":
                byObject = new By.ByXPath(query);
                break;
            case "id":
                byObject = new By.ById(query);
                break;
            case "className":
                byObject = new By.ByClassName(query);
                break;
            case "name":
                byObject = new By.ByName(query);
                break;
            case "linkText":
                byObject = new By.ByLinkText(query);
                break;
            case "partialLinkText":
                byObject = new By.ByPartialLinkText(query);
                break;
            case "tagName":
                byObject = new By.ByTagName(query);
                break;
            default:
                throw new Exception("Option is not available");
        }

        return byObject;
    }
}