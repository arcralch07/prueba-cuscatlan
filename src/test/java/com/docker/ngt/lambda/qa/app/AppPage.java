package com.docker.ngt.lambda.qa.app;

import java.io.IOException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.docker.ngt.lambda.qa.methods.Methods;
import com.docker.ngt.lambda.qa.util.ApiRest;
import com.thoughtworks.gauge.Gauge;


public class AppPage extends Methods {

    private static By TXT_SEARCH, TXT_PURCHEPRICE, TXT_DOWNPAYMENT, TXT_RATE, BTN_CALCULATE, BTN_AMOUNT, BTN_PERCENT, LBL_PAYMENT, CHK_TAXES;
    public static Integer numHabilities, totalmeses;
    public static double cuotaValor, depositoValor;
    public static String [] Habilidad = new String[3];
    public static Integer [] Stats = new Integer[6];
    
    ApiRest apiRest = new ApiRest();

    public AppPage init() throws Exception,IOException {
        //loading selector from properties
        TXT_SEARCH = createBy(System.getenv("BY_XPATH"), System.getenv("SEARCH_TXT"));

        TXT_PURCHEPRICE = createBy(System.getenv("BY_ID"), System.getenv("PURCHEPRICE_TXT"));
        TXT_DOWNPAYMENT = createBy(System.getenv("BY_ID"), System.getenv("DOWNPAYMENT_TXT"));
        TXT_RATE = createBy(System.getenv("BY_ID"), System.getenv("RATE_TXT"));
        BTN_CALCULATE = createBy(System.getenv("BY_ID"), System.getenv("CALCULATE_BTN"));
        BTN_AMOUNT = createBy(System.getenv("BY_ID"), System.getenv("AMOUNT_BTN"));
        BTN_PERCENT = createBy(System.getenv("BY_ID"), System.getenv("PERCENT_BTN"));
        LBL_PAYMENT = createBy(System.getenv("BY_XPATH"), System.getenv("PAYMENT_LBL"));
        CHK_TAXES = createBy(System.getenv("BY_ID"), System.getenv("TAXES_CHK"));
        
        return this;
    }

    public AppPage(WebDriver driver){
        super(driver);
    }

    public AppPage goToPage(String url){
        driver().navigate().to(url);
        return this;
    }

    public AppPage calcularCuotaHipoteca(Double cuota, Double interes, Double deposito, Integer plazo){
        Double monto, tasa;
        depositoValor = (cuota*(deposito/100));
        monto = cuota - depositoValor;
        tasa = (interes/100)/12;
        totalmeses = plazo*12;
        cuotaValor = monto * ((tasa * Math.pow((1 + tasa),totalmeses)) / (Math.pow((1 + tasa),totalmeses) - 1));
        return this;
    }

    public AppPage ingresarDatosHipoteca(Double cuota, Double interes, Double deposito, Integer plazo, Boolean opt){
        sendKeysElement(TXT_PURCHEPRICE, String.valueOf(cuota));
        click(BTN_PERCENT);
        sendKeysElement(TXT_DOWNPAYMENT, String.valueOf(deposito));
        if(opt){
            click(BTN_AMOUNT);
        }
        windowForScroll(0, 500);
        wait(3);
        click(CHK_TAXES);
        sendKeysElement(TXT_RATE, String.valueOf(interes));
        if(totalmeses!=360) {
            click(By.xpath(System.getenv("TERM_LST")));
            wait(2);
            click(By.xpath(System.getenv("TERM_LST")+"/option[@value='"+totalmeses+"']"));
        }
        click(BTN_CALCULATE);
        return this;
    }

    public AppPage verificationCuotaHipoteca(){
        Double total = Double.parseDouble(el(LBL_PAYMENT).getText().replace("$",""));
        System.out.println("% efectividad: " + (total/cuotaValor)*100);
        return this;
    }

    public AppPage consultaHabilidadPokemon(String str){
        sendKeysElement(TXT_SEARCH, str);
        waitForElement(By.xpath("//a[contains(@data-entry,'"+str+"')]/span[2]"));
        click(By.xpath("//a[contains(@data-entry,'"+str+"')]/span[2]"));
        apiRest.ConsultaApiRest(str.toLowerCase());
        verificadorHabilidadesPokemon(str);
        return this;
    }

    public AppPage consultaStatsPokemon(String str){
        verificandoStatsPokemon();
        imprimirDatosPokemon();
        return this;
    }

    public AppPage verificadorHabilidadesPokemon(String str){
        int i=0;
        waitForElement(By.xpath("//a[contains(@data-entry,'"+str+"')]/span[5]"));
        List<WebElement> options = els(By.xpath("//*[@class='abilityentry']//child::a"));
        numHabilities = options.size();
        for(int j=1;j<=numHabilities;j++){
            Habilidad[i]=el(By.xpath("//*[@class='abilityentry']//child::a["+j+"]")).getText();
            i++;
        }
        return this;
    }

    public AppPage verificandoStatsPokemon() {
        int i=0;
        waitForElement(By.xpath("//*[@class='stats']//child::tr[2]/*[@class='stat']"));
        for (int j=2;j<8;j++) {
            Stats[i] = Integer.valueOf(el(By.xpath("//*[@class='stats']//child::tr["+j+"]/*[@class='stat']")).getText());
            i++;
        }
        return this;
    }

    public void screenShot(){
        Gauge.captureScreenshot();
    }

    public AppPage imprimirDatosPokemon(){
        apiRest.verificacionDatosApiRest(Habilidad, Stats, numHabilities);
        
        return this;
    }
    
}